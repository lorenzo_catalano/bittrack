import 'reset-css'
import './css/style.css'




import React from 'react';
import {
  HashRouter,
  Route,
  Link
} from 'react-router-dom';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store'
import CoinboxContainer from './components/coinboxcontainer'

ReactDOM.render((
		<Provider store={store}>
			<HashRouter>
					<div>
            <CoinboxContainer></CoinboxContainer>
					</div>
			</HashRouter>
		</Provider>),
	document.getElementById('root')
);
