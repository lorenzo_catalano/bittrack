import React from 'react';
import { connect } from 'react-redux';
import store from '../store';
import utils from '../utils'
import Loader from './loader'
var baseurl = 'https://api.coinmarketcap.com/v1/ticker/'

class Coinbox extends React.Component {

  componentDidMount(){
    fetch(baseurl + this.props.coin.id + "/?convert=EUR")
    .then(resp => resp.json())
    .then(_ => store.dispatch({type:'COIN_UPDATE',coin:_[0]}))
  }

  render(){
    return (<div className="coinbox">
      <aside>
      <img src={'https://files.coinmarketcap.com/static/img/coins/32x32/'+this.props.coin.id+'.png'}/>
      </aside>
      <section>
      <header>
          <span className="simbolo">
            {this.props.coin.symbol}
          </span>
          <span className="quantita">{this.props.coin.quantity}</span>
      </header>
       <span className="prezzo">
          <label>Prezzo:</label>
          {utils.fmtprice(this.props.coin.price_eur,'€')}
          ({utils.fmtprice(this.props.coin.price_usd,'$')})
        </span>
        <span className="valore">
          <label>Valore:</label>{utils.fmtprice(this.props.coin.value,'€')}
        </span>
        <footer>
          {new Date(parseInt(this.props.coin.last_updated,10)*1000).toLocaleString()}
        </footer>
      </section>
    </div>)
  }
}

export default Coinbox;
