import React from 'react';
import { connect } from 'react-redux';
import store from '../store';
import utils from '../utils'
import Loader from './loader'
var baseurl = 'https://api.coinmarketcap.com/v1/ticker/'

class CoinboxAdd extends React.Component {

  componentDidMount(){
    
  }

  render(){
    return (<div className="coinbox">
      <header>
          <span className="simbolo">
            <select>
              <option value="">Choose</option>
            </select>
          </span>
          <span className="quantita">
            <input type="text"/>
          </span>
      </header>
      <section>
          <span className="prezzo">
            <label>Prezzo:</label>
            {0}
            ({0})
          </span>
          <span className="valore">
            <label>Valore:</label>{utils.fmtprice(0,'€')}
          </span>
      </section>
    </div>)
  }
}

export default CoinboxAdd;
