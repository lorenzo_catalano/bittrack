import React from 'react';
import { connect } from 'react-redux';
import utils from '../utils'
import Loader from './loader'
var baseurl = 'https://api.coinmarketcap.com/v1/ticker/'

class Coinbox extends React.Component {

  componentDidMount(){
    fetch(baseurl + this.props.coin.id + "/?convert=EUR")
    .then(resp => resp.json())
    .then(this.props.coinupdate)
  }

  render(){
    return (<div className="coinbox">
      <header>
          <span className="simbolo">{this.props.coin.symbol}</span>
          <span className="quantita">{this.props.coin.quantity}</span>
      </header>
      <section>
          <span className="prezzo">
            <label>Prezzo:</label>
            {this.props.coin.price_eur}
            ({this.props.coin.price_usd})
          </span>
          <span className="valore">
            <label>Valore:</label>{utils.fmtprice(this.props.coin.value,'€')}
          </span>
      </section>
    </div>)
  }
}
function mstp(store,self){
  return {
    coin:self.coin
  }
}
function mdtp(dispatch){
  return {
    coinupdate : function(_){dispatch({type:'COIN_UPDATE',coin:_[0]})}
  }
}


export default connect(mstp,mdtp)(Coinbox)
