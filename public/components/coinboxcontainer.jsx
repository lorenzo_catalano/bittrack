import React from 'react';
import { connect } from 'react-redux';
import store from '../store';
import Coinbox from './coinbox'
import CoinboxAdd from './coinboxadd'
import utils from '../utils'
var baseurl = 'https://api.coinmarketcap.com/v1/ticker/'

class CoinboxContainer extends React.Component {

  componentDidMount(){
    fetch("/state")
    .then(resp => resp.json())
    .then(_ => store.dispatch({type:'STORE_LOADED',store:_}))
  }

  render(){
    console.log('rendered')
    return (
      <div className="coinboxcontainer">
        <div className="summary">
          {utils.fmtprice(this.props.totalEarned,'€')}
        </div>
        {this.props.coins.map(c=> <Coinbox key={c.id} coin={c}/>)}
      </div>
    )
  }
}

const mapStateToProps = function(store) {
  return {
    coins : store.coins,
    totalEarned : store.totalEarned
  };
}

function mdtp(dispatch){
  return {
    coinupdate : function(_){dispatch({type:'STORE_LOADED',store:_})}
  }
}

export default connect(mapStateToProps,mdtp)(CoinboxContainer)
