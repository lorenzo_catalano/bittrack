import { createStore } from 'redux';
import utils from './utils'


const initialState = {
  totalSpent:0,
  totalEarned:0,
  coins : []
};

var reducer = function(state, action) {
	if (state === undefined) {
    return initialState;
  }
  switch(action.type){
    case 'STORE_LOADED':
      return action.store
      break;
    case 'COIN_UPDATE':
      var x = Object.assign({},state,{
  			coins:state.coins.map(c=>{
          if(c.symbol==action.coin.symbol){
            c.price_eur = action.coin.price_eur
            c.price_usd = action.coin.price_usd
            c.value = utils.calcValue(c)
            c.last_updated = action.coin.last_updated
            return c
          }else{
            return c
          }
        }).sort((c,b)=>b.value-c.value),

  		});
      x.totalEarned = x.coins.reduce((a,b)=>{return a+b.value},0) - x.totalSpent;
      return x
  }

  return state;
}
const store = createStore(reducer);
export default store;
