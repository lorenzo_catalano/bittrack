module.exports = {
  fmtprice : function(x,currency){
    return parseFloat(x,10).toFixed(2) + ' '+currency
  },
  calcValue : function(x){
    return (parseFloat(x.price_eur,10)*x.quantity)
  }
}
