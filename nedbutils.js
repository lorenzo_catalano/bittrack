module.exports = function(db){
  db.findP = function(query){
  return new Promise(function(resolve, reject) {
    db.find(query, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}
db.findOneP = function(query){
  return new Promise(function(resolve, reject) {
    db.findOne(query, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

db.insertP = function(item){
  return new Promise(function(resolve, reject) {
    db.insert(item, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}
}
