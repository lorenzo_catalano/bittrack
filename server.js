'use strict';

var debug = require('debug')('m');
var app = require('./app');

debug.log = console.log.bind(console);

var ip = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '0.0.0.0';
var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3001;

app.set('port', port);
var server = app.listen(port,ip, function() {
  debug('Express server listening on port ' + port);
});
