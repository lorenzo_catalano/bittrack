module.exports = function(db){
  var express = require('express');
  var request = require('request-promise');
  var debug = require('debug')('m');
  var utils = require('../utils')

  var router = express.Router();
  const baseurl = 'https://api.coinmarketcap.com/v1/ticker/'
  const state = {
    totalSpent:1345,
    totalEarned:0,
    coins : [{
      symbol: "BTC",
      id: 'bitcoin',
      quantity: 0.00932073 + 0.0329,
      value:0,
      price_eur:0,
      price_usd:0,
      rank:0
    },
    {
      symbol: "ETH",
      id: 'ethereum',
      quantity: 0.00084968,
      value:0,
      price_eur:0,
      price_usd:0,
      rank:0
    },
    {
      symbol: "LTC",
      id: 'litecoin',
      quantity: 3.4718,
      value:0,
      price_eur:0,
      price_usd:0,
      rank:0
    },
    {
      symbol: "XRP",
      id: 'ripple',
      quantity: 19.98000,
      value:0,
      price_eur:0,
      price_usd:0,
      rank:0
    },
    {
      symbol: "NEO",
      id: 'neo',
      quantity:3.00699,
      value:0,
      price_eur:0,
      price_usd:0,
      rank:0
    },
    {
      symbol: "GAS",
      id: 'gas',
      quantity:0.00843356,
      value:0,
      price_eur:0,
      price_usd:0,
      rank:0
    }
  ]
  }

  router.get('',function(req,res){
    res.send(state)
  })

  router.get('/full',function(req,res){
    var ps = state.coins.map(x=>{
      return request({uri:baseurl + x.id + "/?convert=EUR",json: true})
    })

    Promise.all(ps).then(resps=>{
      var flatresps = resps.map(_ => _[0])
      var coins = state.coins.map(c=>{
        var r = flatresps.find(x=>x.id == c.id)
        if(r){
          c.price_eur = utils.fmtprice(r.price_eur,'€')
          c.price_usd = utils.fmtprice(r.price_usd,'$')
          c.value = utils.calcValue(c)
        }
        return c
        })
        var totalEarned = coins.reduce((a,b)=>{return a+b.value},0) - state.totalSpent;
        res.send({...state,totalEarned,coins})
      }).catch(err=>{
          debug(err)
          res.send(err);
      })

  })

  return router
}
