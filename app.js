var debug = require('debug')('m');
var express = require('express');
var compression = require('compression');
var bodyParser = require('body-parser');

var request = require('request-promise')
var path = require('path');

var Datastore = require('nedb');
var dbpath = './nedb';
var db = new Datastore({filename: dbpath, autoload: true})
require('./nedbutils')(db)
var stateRoute = require('./routes/state')(db);

debug.log = console.log.bind(console);

var app = express();
app.use(compression());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(function(req,res,next){
  debug(`${req.method} ${req.path}`)
  next()
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'dist')));

app.get('/',function(req,res){
  res.render('index')
})
app.use('/state',stateRoute)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err, req, res);
});
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    debug(err);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
